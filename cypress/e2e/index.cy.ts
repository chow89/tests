describe('empty spec', () => {
  it('passes', () => {
    cy.visit('http://localhost:3000');
    cy.get('span.number').should('be.empty');
    cy.get('button').click();
    cy.get('span.number').should('not.be.empty');
  })
})