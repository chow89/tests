import RandomNumberGenerator from "../pages/lib/rng";

let origMathRandom: () => number;
const randomNumber = 0.123;

describe('RNG Unit Test', () => {
    describe('generate', () => {
        beforeAll(() => {
            origMathRandom = Math.random;
            Math.random = () => randomNumber;
        });

        afterAll(() => {
            Math.random = origMathRandom;
        });

        test('with seed', () => {
            expect(RandomNumberGenerator.generate(1)).toBe(123456789);
        });

        test('without seed', () => {
            expect(RandomNumberGenerator.generate()).toBe(123456789 * randomNumber)
        })
    })
});

describe('RNG Integration Test', () => {
    test('generate', () => {
        const n = RandomNumberGenerator.generate();
        expect(n).toBeGreaterThanOrEqual(0);
        expect(n).toBeLessThanOrEqual(123456789)
    })
})
