import type { NextPage } from 'next'
import RandomNumberGenerator from './lib/rng'

const Home: NextPage = () => {
  return (
    <>
      <button onClick={() => buttonClicked()}>Click me</button>
      <span className="number"></span>
    </>
  )
}

const buttonClicked = () => {
  const number = RandomNumberGenerator.generate();
  document.querySelector('span.number')!.innerHTML = number.toString();
}

export default Home
