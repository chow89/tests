export default class RandomNumberGenerator {
    public static generate(seed?: number) {
        if (seed) {
            return seed * 123456789;
        } else {
            return Math.random() * 123456789;
        }
    }
}